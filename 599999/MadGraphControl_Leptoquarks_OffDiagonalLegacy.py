import MadGraphControl.MadGraphUtils
#MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING = { 'central_pdf'     :260000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
#                        'pdf_variations'  :[260000], # pdfs for which all variations (error sets) will be included as weights
#                        'alternative_pdfs':[260000,264000,265000,266000,267000,25100,13100], # pdfs for which only the central set will be included as weights
#                        'scale_variations':[0.5,1.,2.], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
#}
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
import re

##############################

# Set number of events to be generated
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

##############################

mode=0
# set all couplings to 0 to be sure 
couplingDir = {}
for q in ['u', 'c', 't', 'd', 's','b']:
    for l in ['E','M','T','VE','VM','VT']:
        cpStr = "gs"+q+l+"L"
        couplingDir[cpStr] = 0.0
    for l in ['E','M','T']:
        cpStr = "gs"+q+l+"R"
        couplingDir[cpStr] = 0.0

JOname = get_physics_short()

# which of the two LQs in the model?
# lqsd: pdg ID 42, charge -1/3, labelled "down" in the JOs
# lqsu: pdg ID 43, charge 2/3, labelled "up" in the JOs
lqType = ""
lqPDGID = 0
if "_LQu_" in JOname:
    lqType = "lqsu"
    lqPDGID = 43
elif "_LQd_" in JOname:
    lqType = "lqsd"
    lqPDGID = 42
else:
    raise RuntimeError("Cannot find LQ type in JO name.")

if not lqType:
    raise RuntimeError("No LQ type set.")
if not lqPDGID:
    raise RuntimeError("No LQ PDG ID set.")

#Original: 
#matchesMass = re.search("M([0-9]+)\.py", JOname)
matchesMass = re.search("M([0-9]+)", JOname)
if matchesMass is None:
    raise RuntimeError("Cannot find mass string in JO name.")     
else:
    lqmass = float(matchesMass.group(1))

decays = []
JOlist = JOname.split("_")

###########################################################################################
# couplings are calculated as lambda * sqrt(beta) and lambda * sqrt(1 - beta) for decays  #
# in charged and uncharged leptons, respectively                                          #
# default values: beta = 0.5, lambda = 0.3                                                #
# values can be changed by including "lmbd_*p*" and "beta_*p*" in the JO filename         #
# hnd is the fraction of charged leptons that are left-handed, i.e. applied as sqrt(hdn)  #
# for left- and sqrt(1 - hnd) for right-handed leptons, respectively                      #
# default value is hnd = 1.0, can be changed by including "hnd_*p*" in the JO filename    #
###########################################################################################

lambda_cp = 0.3
if "ld" in JOlist: lambda_cp = eval( JOlist[ JOlist.index("ld")+1 ].replace('p','.') )

beta = 0.5
if "beta" in JOlist: beta = eval( JOlist[ JOlist.index("beta")+1 ].replace('p','.') )

hnd = 1.0
if "hnd" in JOlist: hnd = eval( JOlist[ JOlist.index("hnd")+1 ].replace('p','.') )

if beta > 1.0 or beta < 0.0: raise RuntimeError("Unexpected value for beta. Should be in the range from 0 to 1.")
if hnd > 1.0 or hnd < 0.0  : raise RuntimeError("Unexpected value for hnd. Should be in the range from 0 to 1.")

f_uncharged   = ( 1 - beta )**0.5
f_charged     = ( beta )**0.5
f_lefthanded  = ( hnd )**0.5
f_righthanded = ( 1 - hnd )**0.5

cps = []
chargedLepton = ""
downQuark = ""
upQuark = ""
if "_ta_" in JOname:
    chargedLepton = "T"
elif "_mu_" in JOname:
    chargedLepton = "M"
elif "_el_" in JOname:
    chargedLepton = "E"
else:
    raise RuntimeError("No valid lepton for the LQ decay.")
if "_1G_" in JOname:
    downQuark = "d"
    upQuark = "u"
elif "_2G_" in JOname:
    downQuark = "s"
    upQuark = "c"
elif "_3G_" in JOname:
    downQuark = "b"
    upQuark = "t"
else:
    raise RuntimeError("No valid quark generation for the LQ decay.")

if lqPDGID == 43  : cps.extend( [downQuark+chargedLepton+"L", downQuark+chargedLepton+"R", upQuark+"V"+chargedLepton+"L"] )
elif lqPDGID == 42: cps.extend( [downQuark+"V"+chargedLepton+"L", upQuark+chargedLepton+"L", upQuark+chargedLepton+"R"] )

for cp in cps:
    if "L" in cp:
        if "V" in cp: 
            lepton = cp[1]+cp[2]
            couplingDir["gs"+cp] = lambda_cp * f_uncharged
            if f_uncharged: decays.append([cp[0],lepton.lower()])
        else:
            lepton = cp[1]
            couplingDir["gs"+cp] = lambda_cp * f_charged * f_lefthanded
            if f_charged * f_lefthanded: decays.append([cp[0],lepton.lower().replace("m","mu")])
    else:
        lepton = cp[1]
        couplingDir["gs"+cp] = lambda_cp * f_charged * f_righthanded
        if f_charged * f_righthanded and not f_charged * f_lefthanded: decays.append([cp[0],lepton.lower().replace("m","mu")])    
            
#assign correct decays
decayLines = []
for dec in decays:
    #charge 2/3
    if lqPDGID==43:
        if dec[0] in ['u','c','t']:
            if dec[1] in ['e','mu','ta']:
                raise RuntimeError("Fermion charges cannot add up to LQ charge.")
            decayStatement = "decay " + lqType + " > " + dec[0] + " " + dec[1]+"~"
            if dec[0] == "t": decayStatement += ", (t > w+ b, w+ > all all)"
            decayLines.append(decayStatement)
            decayStatement = "decay " + lqType+"~ > " + dec[0]+"~ " + dec[1]
            if dec[0] == "t": decayStatement += ", (t~ > w- b~, w- > all all)"
            decayLines.append(decayStatement)
        elif dec[0] in ['d','s','b']:
            if 'v' in dec[1]:
                raise RuntimeError("Fermion charges cannot add up to LQ charge.")
            decayStatement = "decay " +  lqType + " > " + dec[0] + " " +dec[1]+"+"
            decayLines.append(decayStatement)
            decayStatement = "decay " +  lqType + "~ > " + dec[0]+"~ " + dec[1]+"-"
            decayLines.append(decayStatement)
        else:
            raise RuntimeError("Unexpected quark flavour.")
     
    #charge -1/3     
    if lqPDGID==42:
        if dec[0] in ['d','s','b']:
            if dec[1] in ['e','mu','ta']:
                raise RuntimeError("Fermion charges cannot add up to LQ charge.")
            decayStatement = "decay " +  lqType + " > " + dec[0] + " " + dec[1]
            decayLines.append(decayStatement)
            decayStatement = "decay " +  lqType+"~ > " + dec[0]+"~ " + dec[1]+"~"
            decayLines.append(decayStatement)
        elif dec[0] in ['u','c','t']:
            if 'v' in dec[1]:
                raise RuntimeError("Fermion charges cannot add up to LQ charge.")
            decayStatement = "decay " +  lqType + " > " + dec[0] + " " + dec[1]+"-"
            if dec[0] == "t": decayStatement += ", (t > w+ b, w+ > all all)"
            decayLines.append(decayStatement)
            decayStatement = "decay " +  lqType+"~ > " + dec[0]+"~ " + dec[1]+"+"
            if dec[0] == "t": decayStatement += ", (t~ > w- b~, w- > all all)"
            decayLines.append(decayStatement)
        else:
            raise RuntimeError("Unexpected quark flavour.")

##############################

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

##############################

process = """
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define p = p b b~
define j = j b b~
import model LQmix_NLO
generate p p > {0} {0}~ [all=QCD]
output -f
\n""".format(lqType)

process_dir = new_process(process)

#Original:  
#Fetch default NLO run_card.dat and set parameters
#PDF sets: NNPDF30_nlo_as_0118, _as_0115, _as_0117, _as_0119, _as_121, MMHT2014nlo68cl, CT14nlo , , 264000, 265000, 266000, 267000, 25100, 13100
#extras = { 'nevents'       :1.1*nevents,
#           'pdlabel'       :"'lhapdf'",
#           'parton_shower' :'PYTHIA8',
#           'reweight_scale':'True',
#           'reweight_PDF':'True False False False False False False',
#           'jetalgo':'-1.0',
#           'jetradius':'0.4',
#           'muR_ref_fixed' : lqmass,
#           'muF_ref_fixed' : lqmass,
#           'QES_ref_fixed' : lqmass}

extras = { 'parton_shower' :'PYTHIA8',
           'nevents'       :int(nevents),
           'jetalgo'       :'-1.0',
           'jetradius'     :'0.4',
           'muR_ref_fixed' :lqmass,
           'muF_ref_fixed' :lqmass,
           'QES_ref_fixed' :lqmass}

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=extras)

##############################                                                                                                                     

madspin_card=process_dir+'/Cards/madspin_card.dat' # pointing properly to the card
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~\n"""%runArgs.randomSeed)

for l in decayLines:
    mscard.write(l+'\n')

mscard.write("""launch""")    
mscard.close()

##############################
paramcardname = process_dir+'/Cards/param_card_default.dat'
if not os.access(paramcardname,os.R_OK):
    raise RuntimeError('ERROR: Could not get param card %s'%paramcardname)

try:
    os.remove(process_dir+'/Cards/param_card.dat')
except: 
    pass

oldcard = open(paramcardname,'r')
newcard = open(process_dir+'/Cards/param_card.dat','w')

for line in oldcard:
    if 'MU_R' in line:
        newcard.write('    1 %e # MU_R\n'%(lqmass))
    elif '# Msd' in line:
        newcard.write('   42 %e # Msd \n'%(lqmass))
    elif '# Msu' in line:
        newcard.write('   43 %e # Msu \n'%(lqmass))
    elif 'DECAY  42' in line:
        if lqPDGID==42:
            newcard.write('DECAY  42 Auto # Wsd\n')
        else:
            newcard.write(line)
    elif 'DECAY  43' in line:
        if lqPDGID==43:
            newcard.write('DECAY  43 Auto # Wsu\n')
        else:
            newcard.write(line)
    elif '# gs' in line:
        for cp in couplingDir.keys():
            if cp in line:
                linelist = line.split(' ')
                linelist = ' '.join(linelist).split()
                lhacode = linelist[0]
                newcard.write('    %s %e # %s\n'%(lhacode, couplingDir[cp], cp))
                break
    else:
        newcard.write(line)
oldcard.close()
newcard.close()

##############################

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

##############################

### Py8 shower JO fragments ###
# Shower PDF
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
# NLO shower
include("Pythia8_i/Pythia8_aMcAtNlo.py")

evgenConfig.description = ('Pair production of scalar leptoquarks, {0}, mLQ={1:d}').format(lqType,int(lqmass))
evgenConfig.keywords+=['BSM','exotic','leptoquark','scalar']
evgenConfig.generators += ['aMcAtNlo', 'Pythia8', 'EvtGen']
evgenConfig.process = 'pp -> LQ LQ'
