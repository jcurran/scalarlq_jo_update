# ScalarLQ_JO_update

JOs allowing couplings to one generation of quarks ("_1G_", "_2G_", "_3G_" in the JO name) and one generation of leptons ("_el_", "_mu_", "_ta_" in JOname).

Checked with 

```
setupATLAS -c centos7
asetup AthGeneration,23.6.18,here
Gen_tf.py --ecmEnergy=13600. --maxEvents=100 --randomSeed=123456 --outputEVNTFile=evgen.root --jobConfig=599999
```
